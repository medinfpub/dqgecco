# `dqGecco`

This R package is developed following the `con_contradictions` framework in `dataquieR` package [1]. Its intended use is to check for consistency of interdependent features within the German Corona Consensus (GECCO) dataset.

The modifications to the `con_contradictions` module to suit the GECCO domain specific requirements include:
 * `Domain Specific Logical Rules` implement logical rules to determine the consistency of data elements in a one to multiple-set dependency (e.g. each COVID-19 severity class has specific set of indicators).
 * `Link-back to DB` through a detailed report, identified anomalies can be traced back to the database using their "pheno" keys.
 * `Visualization of anomalies` anomalies are visualized within relevant datapoints and not in isolation to aid comparison with other consistent fields.

All four predefined logics are wrapped under a function conditioned to apply suitable logic to each check-scenario therefore, a separate checklist is not required aside the metadata.

##  Study_data
 The GECCO83 dataset used for the project is available within the NAPKON through the common Use & Access process [2]

##  Meta_data
| ID | A | B | C | v1 | v2 | v3 | v4 | v5 | v6 | A_list | C_sub | D_sub |
|----|----|----|----|----|----|----|----|----|----|----|----|----|
| 1 | Diabetes_Mellitus | Diabetes_Type | Insulin_Medication | with_insulin | without_insulin |NA | NA | NA | NA | not_applicable | not_applicable | not_applicable |
| 2 | Fever_Baseline | Bodytemp_Baseline | NA | Ja | 37.8 | 35 | NA | NA | No | not_applicable | not_applicable | not_applicable |
| 3 | Covid_Severity | NA | NA | Complicated | Critical | Uncomplicated | Ja | Invasive mechanical ventilation | NA | symptoms_list | complicated_indicators | critical_indicators |


##  Installation
### Approach 1: Local Installation
``` r
devtools::install("dqGecco")
```

### Approach 2: GitLab Installation
``` r
devtools::install_gitlab("medinfpub/dqGecco")
```


______
[1] [`dataquieR`](https://gitlab.com/libreumg/dataquier)  
[2] [NAPKON Proskive](https://proskive.napkon.de/)

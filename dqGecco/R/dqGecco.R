## -----------------------------------------------------------------------------
#' Package to check COVID-19 domain specific consistency
#' based on rules derived from EDC and GECCO design
#'
#' @description
#' interdependent variables are identified through GECCO concept analysis
#' inter-dependencies are verified through opinion of experts
#' domain specific logical rules are defined
#' detailed reports (with phenos) are rendered to link back to identified anomalies in DB
#' anomalies are visualized within relevant data-points
#' plots are saved in working directory
#'
#' @param study_data [data.frame] dataset from the mapped GECCO items
#' @param meta_data [data.frame] set of absolutely interdependent elements
#'                               defined by users according to respective names
#'                               in the study_data
#'
#' @return [list]
#' -  AnomalyReport: this report filters all elements with contradictions
#'                   in each case including a unique key that allows a link
#'                   back to the affected elements in the database
#'
#' -  SummaryReport:  this report estimates the absolute and relative frequency
#'                    of contradictions with respect to passed and failed items
#'
#' -  AnomalyPlot:    anomalies identified in each interdependent variable-set are
#'                    visualized within the relevant datapoints and not in isolation to
#'                    present a pictorial evidence and comparison among
#'                    passed and failed value-combinations. These plots are saved
#'                    directly in the working directory as they are generated
#'
#' @export
#' @import dplyr
#' @import ggplot2
#' @import rlang
gecco_contradictions <- function(study_data, meta_data) {
  # render detailed and summarized anomaly reports
  anomalyReport <- gecco_consistency(study_data, meta_data)
  summaryReport <- gecco_report(study_data, meta_data)
  plotAnomaly <- consistency_plot(study_data, meta_data)
  
  return(list(
    AnomalyReport = anomalyReport,
    SummaryReport = summaryReport,
    AnomalyPlot = plotAnomaly
  ))
  
}

# consistency rules between interdependent variables
gecco_rule <- function(study_data, A, B, C, v1, v2, v3, v4, v5, v6, A_list, C_sub, D_sub) {
  X <- study_data
  A_B <- c(A,B)
  A_C <- c(A,C)
  A_B_C <- c(A,B,C)
  
  
  # logical rules
  if (any(stringr::str_detect(A_B_C, "Diabetes"))) {
    anomaly <- X %>%
      mutate(contradiction = ifelse(rowSums(is.na(X[A]) & !is.na(X[B]) & X[C] == v1, na.rm = T) > 0 |
                                      rowSums(is.na(X[A]) & !is.na(X[B]) & X[C] == v2, na.rm = T) > 0 |
                                      rowSums(X[A] == v2 & !is.na(X[B]), na.rm = T) > 0 |
                                      rowSums(X[A] == v2 & X[C] == v1, na.rm = T) > 0 |
                                      rowSums(is.na(X[A]) & !is.na(X[B]), na.rm = T) > 0, 1, 0)) %>%
      select(mnppsd, all_of(A_B_C), contradiction) %>%
      filter(contradiction > 0)
    
  } else if (any(stringr::str_detect(A_B, "Fever"))) {
    anomaly <- X %>%
      mutate(contradiction = ifelse(rowSums(X[A] == v1 & X[B] <= v2 & X[B] >= v3, na.rm = T) > 0 |
                                      rowSums(X[A] == v1 & X[B] <= v2 & X[B] >= v3, na.rm = T) > 0 |
                                      rowSums(X[A] == v6 & X[B] > v2, na.rm = T) > 0 |
                                      rowSums(X[A] == v1 & X[B] <= v2 & X[B] >= v3, na.rm = T) > 0, 1, 0)) %>%
      select(mnppsd, all_of(A_B), contradiction) %>%
      filter(contradiction > 0)
    
  } else if (any(stringr::str_detect(A, "gec_"))) {
    #library(rlang)
    A_list <- unlist(A_list)
    sub_list <- setdiff(A_list, A)
    tmp <- as.name(stringr::str_c(A, "_indicators", sep = ""))
    pickorder = c("Ja", "Nein", "Weiss nicht", "Keine Informationen verfuegbar", NA)
    
    anomaly <- X %>%
      mutate("{{tmp}}" := apply(.[,sub_list], 1, function(x) first(x, order_by = match(x,pickorder)))) %>%
      mutate(contradiction = ifelse(rowSums(.[A] == v1 & .[[tmp]] == v2, na.rm = T) > 0 |
                                    rowSums(.[A] == v2 & .[[tmp]] != v2, na.rm = T) > 0, 1, 0)) %>%
      select(mnppsd, all_of(A_list), contradiction) %>%
      filter(contradiction > 0)
   
  } else if (any(stringr::str_detect(A, "Sever"))) {
    A_list <- unlist(A_list)
    D_sub <- unlist(D_sub)
    v_list <- c(v4,v5,v6)
    anomaly <- X %>%
      mutate(contradiction = ifelse(rowSums(X[A] == v1 & X[C_sub] != v4 & X[B] == v4 & X[C] != v6, na.rm = T) > 0 |
                                      rowSums(X[A] == v1 & X[B] != v4, na.rm = T) > 0 |
                                      rowSums(X[A] == v1 & X[C_sub] != v4, na.rm = T) > 0 |
                                      rowSums(X[A] == v2 & apply(X[,D_sub], 1, function(x) !any(is.na(x))) & apply(X[,D_sub], 1, function(x) !any(x %in% v_list)), na.rm = T) > 0 |
                                      #rowSums(X[A] == v2 & apply(X[,D_sub], 1, function(x) !any(x %in% v_list)), na.rm = T) > 0 |
                                      rowSums(X[A] == v3 & X[C_sub] == v4, na.rm = T) > 0 |
                                      rowSums(X[A] == v3 & X[C] == v6, na.rm = T) > 0 |
                                      #rowSums(X[A] == v2 & X[C_sub] == v4, na.rm = T) > 0 |
                                      rowSums(X[A] == v3 & apply(X[,D_sub], 1, function(x) !any(is.na(x))) &
                                                apply(X[,D_sub], 1, function(x) any(x %in% v_list)), na.rm = T) > 0, 1, 0)) %>%
      select(mnppsd, all_of(A_list), contradiction) %>%
      filter(contradiction > 0)
  } else {
    print("no suitable elements selected")
  }
  
  return(anomaly)
  
}

# render detailed report of all anomalies
#' @export
gecco_consistency <- function(study_data, meta_data){
  
  c_id <- as.list(meta_data$ID)
  summary_df <- list()
  
  for (i in 1:length(c_id)) {
    summary_df[[i]] <- gecco_rule(study_data = study_data,
                                   A = paste(meta_data$A[i]),
                                   B = paste(meta_data$B[i]),
                                   C = paste(meta_data$C[i]),
                                   v1 = paste(meta_data$v1[i]),
                                   v2 = paste(meta_data$v2[i]),
                                   v3 = paste(meta_data$v3[i]),
                                   v4 = paste(meta_data$v4[i]),
                                   v5 = paste(meta_data$v5[i]),
                                   v6 = paste(meta_data$v6[i]),
                                  C_sub = paste(meta_data$C_sub[i]),
                                  A_list <- meta_data$A_list[i],
                                  D_sub <- meta_data$D_sub[i])
  }
  report_list = c("Diabetes_vs_Insulin", "Fever_vs_bodyTemp", 
                  "PD_vs_Indicators", "CVD_vs_Indicators", "Symptoms_vs_Indicators",
                  "Chronic_Neuro_vs_Indicators","Covid_Severity_vs_Indicators")
  names(summary_df) = report_list
  return(summary_df)
}

# render summary report
#' @export
gecco_report <- function(study_data, meta_data) {
  X <- study_data
  cl <- as.list(meta_data$ID)
  report <- gecco_consistency(X, meta_data)
  
  # prepare summary report frame
  summary_df <- data.frame(
    Features = rep(NA, length(cl)),
    Passed_Fields = rep(NA, length(cl)),
    Failed_Fields = rep(NA, length(cl)),
    Passed_Rate = rep(NA, length(cl))
  )
  
  # summary
  for(i in 1:length(report)) {
    summary_df[i, 1] <- names(report)[i]
    summary_df[i, 2] <- NROW(X) - sum(report[[i]]["contradiction"])
    summary_df[i, 3] <- sum(report[[i]]["contradiction"])
    summary_df[i, 4] <- round((summary_df[i, 2] / NROW(X))*100, digits = 2)
  }
  return(summary_df)
}


# consistency checks in ggplot
plot_rule <- function(study_data, A, B, C, v1, v2, v3, v4, v5, v6, A_list) {
  #library(rlang)
  A_list <- unlist(A_list)
  A_B <- c(A,B)
  sub_list <- setdiff(A_list, A)
  cross_df <- study_data
  pickorder = c("Ja", "Nein", "Weiss nicht", "Keine Informationen verfuegbar", NA)
  
  if (any(stringr::str_detect(A_B, "Fever"))) {
    cross_df %>%
      select({A}, {B}) %>%
      count(!!sym(A), !!sym(B)) %>%
      rename(Freq = n) %>%
      mutate(Consistency = ifelse(rowSums(.[A] == v1 & .[B] <= v2 & .[B] >= v3, na.rm = T) > 0 |
                                    rowSums(.[A] == v6 & .[B] > v2, na.rm = T) > 0 |
                                    #rowSums(.[A] == v1 & .[B] <= v2 & .[B] >= v3 , na.rm = T) > 0 |
                                    rowSums(.[A] == v1 & .[B] <= v2 & .[B] >= v3, na.rm = T) > 0, "No", "Yes"))
    
  } else if (any(stringr::str_detect(A, "gec_"))) {
    tmp <- as.name(stringr::str_c(A, "_indicators", sep = ""))
    cross_df %>%
      mutate("{{tmp}}" := apply(.[,sub_list], 1, function(x) first(x, order_by = match(x,pickorder)))) %>%
      select({A}, {{tmp}}) %>%
      count(!!sym(A), {{tmp}}) %>%
      rename(Freq = n) %>%
      mutate(Consistency = ifelse(rowSums(.[A] == v1 & .[[tmp]] == v2, na.rm = T) > 0 |
                                    rowSums(.[A] == v2 & .[[tmp]] != v2, na.rm = T) > 0, "No", "Yes"))
    
  } 
}

# plot inconsistency and save in wkdir
#' @export
consistency_plot <- function(study_data, meta_data) {
  #library(ggplot2)
  cl <- as.list(meta_data$ID)
  gec_plot <- list()
  
  for(i in 1:length(cl)) {
    X <- plot_rule(study_data = study_data,
                    A = paste(meta_data$A[i]),
                    B = paste(meta_data$B[i]),
                    C = paste(meta_data$C[i]),
                    v1 = paste(meta_data$v1[i]),
                    v2 = paste(meta_data$v2[i]),
                    v3 = paste(meta_data$v3[i]),
                    v4 = paste(meta_data$v4[i]),
                    v5 = paste(meta_data$v5[i]),
                    v6 = paste(meta_data$v6[i]),
                    A_list <- meta_data$A_list[i])
    if (any(stringr::str_detect(colnames(X), "gec_"))) {
      gec_plot[[i]] <- ggplot(X, aes(x = get(colnames(X[2])), y=get(colnames(X[1])), fill = Consistency)) +
        geom_tile() +
        labs(x = colnames(X[2]), y = colnames(X[1])) +
        geom_text(aes(label= Freq), colour="white", check_overlap = TRUE) +
        theme_classic(base_size = 14)
      save_plot = paste("plot_", i, ".png", sep="")
      png(save_plot, width = 700,height = 480)
      print(gec_plot[[i]])
      dev.off()
    } else if (any(stringr::str_detect(colnames(X), "Fever"))) {
      gec_plot[[i]] <- ggplot(X, aes(x = get(colnames(X[1])), y=get(colnames(X[2])), fill = Consistency)) +
        geom_point(aes(color = Consistency)) +
        labs(x = colnames(X[1]), y = colnames(X[2])) +
        theme_classic(base_size = 14)
      save_plot = paste("plot_", i, ".png", sep="")
      png(save_plot, width = 450,height = 650)
      print(gec_plot[[i]])
      dev.off()
    }
  }
  
}

